# Static Web Page Helm Chart
## Description
This helm chart helps deploying static websites like nginx based or similar instances to kubernetes cluster.
## Installation
```console
helm install -n <NAMESPACE> web-static .
```
### Parameters
|Name|Description|Value|
|:----|:----|:----:|
|`replicaCount`|Pod's replicas|`"1"`|
|`nameOverride`|String to partially override common.names.fullname template (will maintain the release name)|`""`|
|`fullnameOverride`|String to fully override common.names.fullname template|`""`|
|`image.repository`|Name of container image|`""`|
|`image.pullPolicy`|Container image's pull policy|`Always`|
|`image.tag`|Container image's tag|`latest`|
|`registryCredentials.registry`|Registry name|`""`|
|`registryCredentials.username`|Registry username|`""`|
|`registryCredentials.password`|Regsitry password|`""`|
|`imagePullSecrets`|Specify registry secret names as an array|`[]`|
|`podAnnotations`|Pod annotation for app name|`{}`|
|`jobAnnotations`|Job annotation for app name|`{}`|
|`serviceAccount.create`|To create a service account|`true`|
|`serviceAccount.annotations`|ServiceAccount Annotations|`{}`|
|`serviceAccount.name`|Service account's name|`""`|
|`podSecurityContext`|pod security context parameters|`{}`|
|`securityContext`|security context parameters|`{}`|
|`deployment.strategy.enabled`|enable specific deployment strategy|`"false"`|
|`deployment.strategy.type`|deployment strategy type|`"Recreate"`|
|`deployment.containerPort`|container port|`80`|
|`deployment.containerPortName`|container port name|`"http"`|
|`deployment.containerPortProtocol`|container port protocol|`"TCP"`|
|`deployment.livenessProbe`|liveness probe parameters|`{}`|
|`ingress.enabled`|enable ingress|`"false"`|
|`ingress.className`|ingress classname|`"nginx"`|
|`ingress.annotations`|ingress annotations|`{}`|
|`ingress.hosts`|ingress hosts|`[]`|
|`ingress.tls`|ingress tls|`[]`|
|`resources.limits`|resources limits|`{}`|
|`resources.requests`|resources requests|`{}`|
|`autoscaling.enabled`|enable autoscaling|`"false"`|
|`autoscaling.minReplicas`|min replicas|`1`|
|`autoscaling.maxReplicas`|max replicas|`2`|
|`autoscaling.targetCPUUtilizationPercentage`|target CPU utilization percentage|`80`|
|`autoscaling.targetMemoryUtilizationPercentage`|target memory utilization percentage|`90`|
|`nodeSelector`|node selector attributes|`{}`|
|`tolerations`| tolerations attributes|`[]`|
|`affinity`|Affinity attributes|`{}`|